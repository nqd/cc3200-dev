
How to start
----------------------------
To avoid sudo when accessing the /dev/ttyUSB*
```
sudo addgroup <your user name> dialout
sudo service udev restart
```

Clone the project, with submodule

For kernel >= 3.12 (Ubuntu 14.04), create (as root user) the file /etc/udev/rules.d/99-ti-launchpad.rules.

Add the following lines to this file:
```
# CC3200 LaunchPad
SUBSYSTEM=="usb", ATTRS{idVendor}=="0451", ATTRS{idProduct}=="c32a", MODE="0660", GROUP="dialout", 
RUN+="/sbin/modprobe ftdi-sio" RUN+="/bin/sh -c '/bin/echo 0451 c32a > /sys/bus/usb-serial/drivers/ftdi_sio/new_id'"
```
The board should have enumberated as /dev/ttyUSB{0,1}. We will use ttyUSB1 for UART backchannel

Install GNU Tools for ARM Embeded Processor: gcc-arm-none-eabi, gdb-arm-none-eabi

Install OpenOCD for onchip debugging: openocd (v0.7)

Run your first example
-------------------------

- flashing

- debugging

Note
------------------------
- test with sdk 1.1.0
- cc3200prog is extracted from energia-0101E0016
